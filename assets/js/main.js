// firebase.auth().onAuthStateChanged((user) => {
//   if (user) {
//     console.log('berhasil login');
//   } else {
//   }
// });

// const googleLogin = (e) => {
//   const provider = new firebase.auth.GoogleAuthProvider();
//   firebase.auth().signInWithRedirect(provider);
//   e.preventDefault();
// }

// const btnLogin = document.querySelector(`[data-button="login"]`);
// btnLogin.onclick = (e) => googleLogin(e);

// const btnLogout = document.querySelector(`[data-button="logout"]`);
// btnLogout.onclick = () => firebase.auth().signOut().then(() => 
// {
//   console.log('keluar')
// }).catch((err) => alert(err));

const container = document.querySelector(".content");

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        Dashboard(user);
    } else {
        Landing();
    }
});

const logoutBtn = () => firebase.auth().signOut().then(() => alert('Logged Out')).catch((err) => alert(err));

const Dashboard = () => {
    document.body.classList.add('dashboard-page');
    document.body.classList.remove('login-page');

    const navButton = document.createElement('div');
    navButton.innerHTML = (`
      <button onClick="logoutBtn()" class="btn-logout">LOGOUT</button>
    `);

    const element = document.createElement('div');
    element.classList.add('container');
    element.innerHTML = (`
      <div class="card-columns">
        <div class="card card-1">
          <h3 class="card-title">Transport</h3>
          <div class="card-body d-flex flex-wrap">
            <div class="col-4">
              <div class="app-menu">
                <a href="https://app.cartoncloud.com/" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-carton-cloud.svg" alt="ic app">
                  </div>
                  <p>Carton Cloud</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" data-toggle="modal" data-target="#modalServiceAreas">
                  <div class="img">
                    <img src="./assets/icons/ic-service-areas.svg" alt="ic app">
                  </div>
                  <p>Service Areas</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-custom-labels.svg" alt="ic app">
                  </div>
                  <p>Custom <br> Labels</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" data-toggle="modal" data-target="#modalRegionalSchedule">
                  <div class="img">
                    <img src="./assets/icons/ic-regional-schedule.svg" alt="ic app">
                  </div>
                  <p>Regional Schedule</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.com.au/transport-form/">
                  <div class="img">
                    <img src="./assets/icons/ic-get-a-quote.svg" alt="ic app">
                  </div>
                  <p>Get a Quote</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-fuel-service-levy.svg" alt="ic app">
                  </div>
                  <p>Fuel Service Levy</p>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="card card-2">
          <h3 class="card-title">Help Centre</h3>
          <div class="card-body d-flex flex-wrap">
            <div class="col-4">
              <div class="app-menu">
                <a href="https://faq.chill.com.au/tickets-view" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-raise-ticket.svg" alt="ic app">
                  </div>
                  <p>Raise a Ticket</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://faq.chill.com.au/en/knowledge" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-faqs.svg" alt="ic app">
                  </div>
                  <p>FAQ's</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.com.au/terms-condition/" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-tncs.svg" alt="ic app">
                  </div>
                  <p>T&C's</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://faq.chill.com.au/en/knowledge/accreditation" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-credentials.svg" alt="ic app">
                  </div>
                  <p>Credentials</p>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="card card-3">
          <h3 class="card-title">Rental</h3>
          <div class="card-body d-flex flex-wrap">
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.com.au/refrigerated-rentals/" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-rental-booking.svg" alt="ic app">
                  </div>
                  <p>Rental Booking</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://app.hubspot.com/documents/3990289/view/297209415?accessId=44715c" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-choose-equipment.svg" alt="ic app">
                  </div>
                  <p>Choose Your Equipment</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://faq.chill.com.au/rental-form" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-get-a-quote.svg" alt="ic app">
                  </div>
                  <p>Get a Quote</p>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="card card-4">
          <h3 class="card-title">Warehouse</h3>
          <div class="card-body d-flex flex-wrap">
            <div class="col-4">
              <div class="app-menu">
                <a href="https://app.cartoncloud.com/" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-carton-cloud.svg" alt="ic app">
                  </div>
                  <p>Carton Cloud</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-custom-labels.svg" alt="ic app">
                  </div>
                  <p>Custom <br> Labels</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.com.au/storage-form/" target="_blank">
                  <div class="img">
                    <img src="./assets/icons/ic-get-a-quote.svg" alt="ic app">
                  </div>
                  <p>Get a Quote</p>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="card card-5">
          <h3 class="card-title">Accounts</h3>
          <div class="card-body d-flex flex-wrap">
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.applyeasy.com.au/credit" target="_blank">
                  <div class="img small">
                    <img src="./assets/icons/ic-account-application.svg" alt="ic app">
                  </div>
                  <p>Account Application</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" data-toggle="modal" data-target="#modalMakePayment">
                  <div class="img small">
                    <img src="./assets/icons/ic-make-payment.svg" alt="ic app">
                  </div>
                  <p>Make a Payment</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" target="_blank">
                  <div class="img small">
                    <img src="./assets/icons/ic-accident.svg" alt="ic app">
                  </div>
                  <p>Intent to Claim</p>
                </a>
              </div>
            </div>
          </div>
        </div>
        
        <div class="card card-6">
          <h3 class="card-title">Xp / Promo</h3>
          <div class="card-body d-flex flex-wrap">
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.com.au/promotion-logistics-form/" target="_blank">
                  <div class="img small">
                    <img src="./assets/icons/ic-forms.svg" alt="ic app">
                  </div>
                  <p>Build a Brief</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="#" data-toggle="modal" data-target="#modalDownloadsAssets">
                  <div class="img small">
                    <img src="./assets/icons/ic-downloads.svg" alt="ic app">
                  </div>
                  <p>Downloads</p>
                </a>
              </div>
            </div>
            <div class="col-4">
              <div class="app-menu">
                <a href="https://chill.com.au/wp-content/uploads/2022/12/A4-Delivery-Address-Label-Chill-Locations-221212.pdf
                " target="_blank">
                  <div class="img small">
                    <img src="./assets/icons/ic-custom-labels.svg" alt="ic app">
                  </div>
                  <p>Address Labels</p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modalServiceAreas" tabindex="-1" aria-labelledby="modalServiceAreas" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modalServiceAreasLabel">Service Areas</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="./assets//icons/ic-x-modal.svg" alt="">
            </button>
            </div>
            <div class="modal-body">
              <div class="row d-flex mx-0">
                <div class="col-4">
                  <div class="app-menu">
                    <a href="https://chillcrew.maps.arcgis.com/apps/webappviewer/index.html?id=1eac2978c7e34f02915f9948ee0a6d02" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-nsw.svg" alt="ic app">
                      </div>
                      <p>NSW</p>
                    </a>
                  </div>
                </div>
                <div class="col-4">
                  <div class="app-menu">
                    <a href="https://chillcrew.maps.arcgis.com/apps/webappviewer/index.html?id=7bf4a6daba674dc0b5c7883819c520bb" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-vic.svg" alt="ic app">
                      </div>
                      <p>VIC</p>
                    </a>
                  </div>
                </div>
                <div class="col-4">
                  <div class="app-menu">
                    <a href="https://chillcrew.maps.arcgis.com/apps/webappviewer/index.html?id=8d2250d0bdc24e63a4bd37918a443b93" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-qld.svg" alt="ic app">
                      </div>
                      <p>QLD</p>
                    </a>
                  </div>
                </div>
                <div class="col-4">
                  <div class="app-menu">
                    <a href="https://chillcrew.maps.arcgis.com/apps/webappviewer/index.html?id=0767ae3b49494470b49910d929467449" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-wa.svg" alt="ic app">
                      </div>
                      <p>WA</p>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modalRegionalSchedule" tabindex="-1" aria-labelledby="modalRegionalSchedule" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalRegionalScheduleLabel">Regional Schedule</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="./assets//icons/ic-x-modal.svg" alt="">
              </button>
            </div>
            <div class="modal-body">
              <div class="row d-flex mx-0">
                <div class="col-4">
                  <div class="app-menu">
                    <a href="#" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-nsw.svg" alt="ic app">
                      </div>
                      <p>NSW</p>
                    </a>
                  </div>
                </div>
                <div class="col-4">
                  <div class="app-menu">
                    <a href="#" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-vic.svg" alt="ic app">
                      </div>
                      <p>VIC</p>
                    </a>
                  </div>
                </div>
                <div class="col-4">
                  <div class="app-menu">
                    <a href="#" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-qld.svg" alt="ic app">
                      </div>
                      <p>QLD</p>
                    </a>
                  </div>
                </div>
                <div class="col-4">
                  <div class="app-menu">
                    <a href="#" target="_blank">
                      <div class="img">
                        <img src="./assets/icons/ic-wa.svg" alt="ic app">
                      </div>
                      <p>WA</p>
                    </a>
                  </div>
                </div>
              </div>
          </div>
          </div>
        </div>
      </div>

      <div class="modal modal-payment fade" id="modalMakePayment" tabindex="-1" aria-labelledby="modalMakePayment" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalMakePaymentLabel">Make a Payment</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="./assets//icons/ic-x-modal.svg" alt="">
              </button>
            </div>
            <div class="modal-body">
              <div class="phone-list">
                <div class="phone-item">
                  <p>Sydney Office</p>
                  <a href="#" class="call-number">
                    <img src="./assets/icons/ic-phone.svg" alt="">
                    <p>(02) 9907 7099</p>
                  </a>
                </div>
                <div class="phone-item">
                  <p>Melbourne Office</p>
                  <a href="#" class="call-number">
                    <img src="./assets/icons/ic-phone.svg" alt="">
                    <p>(03) 8774 6412</p>
                  </a>
                </div>
                <div class="phone-item">
                  <p>Brisbane Office</p>
                  <a href="#" class="call-number">
                    <img src="./assets/icons/ic-phone.svg" alt="">
                    <p>(07) 3733 0888</p>
                  </a>
                </div>
                <div class="phone-item">
                  <p>Perth Office</p>
                  <a href="#" class="call-number">
                    <img src="./assets/icons/ic-phone.svg" alt="">
                    <p>(08) 6103 1313</p>
                  </a>
                </div>
                <div class="phone-item">
                  <p>Accounts (option 4)</p>
                  <a href="#" class="call-number">
                    <img src="./assets/icons/ic-phone.svg" alt="">
                    <p>(02) 9907 7099</p>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal modal-downloads fade" id="modalDownloadsAssets" tabindex="-1" aria-labelledby="modalDownloadsAssets" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
          <div class="modal-content">
              <div class="modal-header">
              <h5 class="modal-title" id="modalDownloadsAssetsLabel">Make a Payment</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <img src="./assets//icons/ic-x-modal.svg" alt="">
              </button>
              </div>
              <div class="modal-body">
                  <div class="accordion" id="accordionExample">
                      <div class="card">
                        <div class="card-header" id="headingOne">
                              <button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <h2 class="mb-0">Vehicle</h2>
                                  <img src="./assets/icons/ic-accordion.svg" alt="">
                              </button>
                        </div>
                    
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                          <div class="card-body">
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Whipee</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>VMOB</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>2P Mercedes Sprinter</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>3P Mercedes Sprinter</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Iveco</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Combi</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>6 Pallet Truck</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>10 Pallet Truck</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>12 Pallet Truck</span>
                              </a>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="headingTwo">
                          <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              <h2 class="mb-0">Promotional Sampling Equipment</h2>
                              <img src="./assets/icons/ic-accordion.svg" alt="">
                          </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                          <div class="card-body">
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Opti Cooler</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Push Cart</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Demo Cart</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Bike & Cart Combo</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Ice Cream Cart</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Mega Bin</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>40L Chilly Been</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Ice tube</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Trestle Table</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Collapsible Crate</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Marquee</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Generator</span>
                              </a>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="headingThree">
                          <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              <h2 class="mb-0">Other</h2>
                              <img src="./assets/icons/ic-accordion.svg" alt="">
                          </button>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                          <div class="card-body">
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Opti Cooler</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Push Cart</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Demo Cart</span>
                              </a>
                              <a href="#" class="download-item">
                                  <img src="./assets/icons/ic-back.svg" alt="">
                                  <span>Bike & Cart Combo</span>
                              </a>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    `);

    // const logoutBtn = element.querySelector(`[data-button="logout"]`);

    const navbar = document.querySelector(".nav-button");
    navbar.appendChild(navButton);
    
    container.innerHTML = '';
    container.appendChild(element);
}

const Landing = () => {
    document.body.classList.add('login-page');
    document.body.classList.remove('dashboard-page');
    const element = document.createElement('div');
    element.classList.add('container');
    element.innerHTML = (`
        <h2>Welcome to Chill Dashboard</h2>
        <p>To login you need to have a <span>@chill.com.au</span> account</p>
        <button data-button="login" class="btn btn-login">
          <img src="./assets/icons/ic-google.svg" alt=""> Login with your account
        </button>
    `);

    const navbar = document.querySelector(".nav-button");
    navbar.innerHTML = '';

    container.innerHTML = '';
    container.appendChild(element);
    container.classList.add("text-center")

    const loginBtn = element.querySelector(`[data-button="login"]`);
    loginBtn.onclick = () => loginGoogle();

    const loginGoogle = () => {
        const provider =  new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithRedirect(provider);
    }
}